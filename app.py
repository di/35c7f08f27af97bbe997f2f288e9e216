import os
from flask import Flask, request
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '.'

@app.route('/', methods=['POST'])
def upload_file():
    file = request.files['filedata']
    if file:
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return 'it worked'
    return 'no file'

@app.route('/', methods=['GET'])
def index():
    return 'it works'

if __name__ == '__main__':
    app.run('0.0.0.0', port=80, debug=True)